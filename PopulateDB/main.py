import random, os

tablename = "test1"
profilename = "BBDGRADGP1"
region = "af-south-1"
readfilename = "Jokes.txt"
writefilename = "populateDBdynamic"

tags = ["#funnyguyz", "#funnytext", "#mobilelegendsfunny", "#funnydankmemes", "#funnygirls", "#catfunny", "#funny_post", "#unfunnymemes", "#funnydad", "#itsfunny", "#hilariousmemes", "#funnyfail", "#funnypicture", "#memes4life", "#hillarious","#jokesonyou", "#jojoke", "#jokerart", "#joker123casino", "#joker128", "#jokerfans", "#souvenirmojokerto", "#bestjokes", "#purworejokeren", "#issajoke", "#sidoarjokekinian", "#ukjokes", "#jokesup", "#jokergaming", "#msglowmojokerto", "#jokergang", "#memes😂", "#funnyposts", "#funnymemez"]
output_begin = 'var AWS = require("aws-sdk");\nvar credentials = new AWS.SharedIniFileCredentials({profile: "'+profilename+'"});\nAWS.config.credentials = credentials;\nAWS.config.update({region: "'+region+'"});\nvar ddb = new AWS.DynamoDB({apiVersion: "2012-08-10"});\n\n\nvar params = {\n  RequestItems: {\n    "'+tablename+'": [\n'
output_end='    ]\n  }\n};\n\n\nddb.batchWriteItem(params, function(err, data) {\n  if (err) {\n    console.log("Error", err);\n  } else {\n    console.log("Success", data);\n  }\n});\n'
batch_items=[]

###### Read from file
print("Reading items from "+readfilename)
fileR = open(readfilename, 'r')
Lines = fileR.readlines()
count = 0
for line in Lines:
     count += 1
     #print("Line{}: {}".format(count, line.strip()))
     batch_items.append('       {\n         PutRequest: {\n           Item: {\n             "ID": { "N": "'+str(count)+'" },\n               "Creator": {"S": "FunnyGuy'+str(random.randint(100,999))+'"},\n               "Text": { "S": "'+line.strip()+'" },\n               "Tags": { "S": "['+tags[random.randint(0,len(tags)-1)]+', '+tags[random.randint(0,len(tags)-1)]+', '+tags[random.randint(0,len(tags)-1)]+']"},\n               "CreationTime": { "S": "2022-'+str(random.randint(1,6))+'-'+str(random.randint(1,28))+'T'+str(random.randint(0,23))+':'+str(random.randint(1,59))+'+02:00"}\n           }\n         }\n       },\n')
fileR.close()
print("Success. All items read from file. Initiating AWS Upload process...")
######

###### Generate/Invoke script per 25 items
print("---------------------------")
count = 0
numinvocations=0
while(count<len(batch_items)):
     output = ""
     for x in range(1, 26):
          if (count<len(batch_items)):
           output=output+batch_items[count]
           count=count+1
     output= output[:-2] + "\n"
     fileW = open(writefilename+str(numinvocations)+".js", 'w')
     fileW.writelines(output_begin+output+output_end)
     fileW.close()
     print("Script generated. Invoking script to upload jokes to AWS...")
     os.system("node "+writefilename+str(numinvocations)+".js")
     numinvocations=numinvocations+1
print("---------------------------\nRun successful. Joke count: "+str(len(batch_items))+"\nFile Gens + Invocations: "+str(numinvocations))
######


