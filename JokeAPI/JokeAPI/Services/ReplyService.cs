﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using JokeAPI.Data;
using JokeAPI.Models;

namespace JokeAPI.Services
{
    public class ReplyService : IReplyService
    {
        private readonly IDynamoDBContext context;
        private readonly IAmazonDynamoDB client;
        private readonly ITableCreator tableCreator;
        private readonly string tableName = "Reply";

        public ReplyService(IDynamoDBContext context, IAmazonDynamoDB client, ITableCreator tableCreator)
        {
            this.context = context;
            this.client = client;
            this.tableCreator = tableCreator;
            this.tableCreator.TryCreateReplyTable();
        }

        public async Task<Reply> CreateReply(Reply reply)
        {
            throw new NotImplementedException();
        }

        public async Task<Reply> DeleteReply(Reply reply)
        {
            throw new NotImplementedException();
        }

        public async Task<Joke> GetReplyById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Reply>> GetReplies(int commentId)
        {
            throw new NotImplementedException();
        }

        public async Task<Reply> UpdateReply(Reply reply)
        {
            throw new NotImplementedException();
        }
    }
}
