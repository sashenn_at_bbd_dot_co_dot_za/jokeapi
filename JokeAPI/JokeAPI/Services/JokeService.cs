﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using JokeAPI.Data;
using JokeAPI.Models;
using System.Collections.Generic;
using System.Text.Json;

namespace JokeAPI.Services
{
    public class JokeService : IJokeService
    {
        private readonly IDynamoDBContext context;
        private readonly IAmazonDynamoDB client;
        private readonly ITableCreator tableCreator;
        private readonly string tableName = "Joke";

        public JokeService(IDynamoDBContext context, IAmazonDynamoDB client, ITableCreator tableCreator)
        {
            this.context = context;
            this.client = client;
            this.tableCreator = tableCreator;
            this.tableCreator.TryCreateJokeTable();
        }

        public async Task<Joke?> CreateJoke(Joke joke)
        {
            try
            {
                await context.SaveAsync(joke);
                return await context.LoadAsync(joke);
            }
            catch
            {
                return null;
            }
        }

        public async Task<Joke?> DeleteJoke(Joke joke)
        {
            try
            {
                await context.DeleteAsync<Joke>(joke);
                return joke;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Joke?> GetJokeById(int id)
        {
            Console.WriteLine(await context.LoadAsync<Joke>(id));
            return await context.LoadAsync<Joke>(id);
        }

        public async Task<List<Joke>?> GetJokes()
        {
            var request = new ScanRequest
            {
                TableName = tableName
            };

            var response = await client.ScanAsync(request);

            var jokes = new List<Joke>();

            foreach(var item in response.Items)
            {
                jokes.Add(AttributeValueToJoke(item));
            }

            return jokes;
        }

        public async Task<Joke?> UpdateJoke(Joke joke)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Joke>?> GetJokesByTag(string tag)
        {
            var request = new ScanRequest
            {
                TableName = tableName
                //ScanFilter = new 
            };

            var response = await client.ScanAsync(request);

            var jokes = new List<Joke>();

            foreach (var item in response.Items)
            {
                jokes.Add(AttributeValueToJoke(item));
            }

            return jokes;
        }

        private Joke AttributeValueToJoke(Dictionary<string, AttributeValue> item)
        {
            var joke = new Joke();

            foreach (KeyValuePair<string, AttributeValue> pair in item)
            {
                var key = pair.Key;
                switch (key)
                {
                    case "Id":
                        joke.Id = int.Parse(pair.Value.N);
                        break;
                    case "Creator":
                        joke.Creator = pair.Value.S;
                        break;
                    case "Text":
                        joke.Text = pair.Value.S;
                        break;
                    case "Tags":
                        joke.Tags = pair.Value.SS;
                        break;
                    case "CreationTime":
                        joke.CreationTime = DateTime.Parse(pair.Value.S);
                        break;
                }
            }

            return joke;
        }
    }
}
