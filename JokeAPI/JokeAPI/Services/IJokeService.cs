﻿using JokeAPI.Models;

namespace JokeAPI.Services
{
    public interface IJokeService
    {
        Task<Joke?> CreateJoke(Joke joke);
        Task<Joke?> UpdateJoke(Joke joke);
        Task<Joke?> DeleteJoke(Joke joke);
        Task<List<Joke>?> GetJokes();
        Task<Joke?> GetJokeById(int id);
        Task<List<Joke>?> GetJokesByTag(string tag);
    }
}
