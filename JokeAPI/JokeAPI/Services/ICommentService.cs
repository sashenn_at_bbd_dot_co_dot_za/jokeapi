﻿using JokeAPI.Models;

namespace JokeAPI.Services
{
    public interface ICommentService
    {
        Task<Comment> CreateComment(Comment comment);
        Task<Comment> UpdateComment(Comment comment);
        Task<Comment> DeleteComment(Comment comment);
        Task<List<Comment>> GetComments(int jokeId);
        Task<Comment> GetCommentById(int id);
    }
}
