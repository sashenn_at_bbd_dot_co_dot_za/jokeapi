﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using JokeAPI.Data;
using JokeAPI.Models;

namespace JokeAPI.Services
{
    public class CommentService : ICommentService
    {
        private readonly IDynamoDBContext context;
        private readonly IAmazonDynamoDB client;
        private readonly ITableCreator tableCreator;
        private readonly string tableName = "Comment";

        public CommentService(IDynamoDBContext context, IAmazonDynamoDB client, ITableCreator tableCreator)
        {
            this.context = context;
            this.client = client;
            this.tableCreator = tableCreator;
            this.tableCreator.TryCreateCommentTable();
        }

        public async Task<Comment?> CreateComment(Comment comment)
        {
            await context.SaveAsync(comment);
            try
            {
                return await context.LoadAsync(comment);
            }
            catch
            {
                return null;
            }
        }

        public async Task<Comment?> DeleteComment(Comment comment)
        {
            try
            {
                await context.DeleteAsync<Comment>(comment);
                return comment;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Comment?> GetCommentById(int id)
        {
            return await context.LoadAsync<Comment>(id);
        }

        public async Task<List<Comment>?> GetComments(int jokeId)
        {
            var getBatch = context.CreateBatchGet<Joke>();
            getBatch.AddKey(0);
            getBatch.AddKey(2);
            await getBatch.ExecuteAsync();
            return getBatch.Results.ToList();
        
        }

        public async Task<Comment?> UpdateComment(Comment comment)
        {
            throw new NotImplementedException();
        }
    }
}
