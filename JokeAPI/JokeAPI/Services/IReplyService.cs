﻿using JokeAPI.Models;

namespace JokeAPI.Services
{
    public interface IReplyService
    {
        Task<Reply> CreateReply(Reply reply);
        Task<Reply> UpdateReply(Reply reply);
        Task<Reply> DeleteReply(Reply reply);
        Task<List<Reply>> GetReplies(int commentId);
        Task<Joke> GetReplyById(int id);
    }
}
