﻿using JokeAPI.Models;
using System.Text.Json;

namespace JokeAPI.Modules
{
    public abstract class Serializable
    {
        public string ToJson()
        {
            return JsonSerializer.Serialize(this);
        }

        public static object? FromJson<T>(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            return JsonSerializer.Deserialize<T>(json);
        }
    }
}
