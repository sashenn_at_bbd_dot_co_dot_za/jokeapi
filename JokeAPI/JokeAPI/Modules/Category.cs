﻿using System.ComponentModel;

namespace JokeAPI.Modules
{
    public enum Category
    {
        [Description("Not safe for work")]
        NSFW,
        [Description("Insensitive jokes")]
        DarkHumour
    }
}
