﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

namespace JokeAPI.Data
{
    public class TableCreator : ITableCreator
    {
        private readonly IAmazonDynamoDB client;

        public TableCreator(IAmazonDynamoDB client)
        {
            this.client = client;
        }

        public async void TryCreateJokeTable()
        {
            var table = "Joke";
            try
            {
                await client.DescribeTableAsync(table);
            } 
            catch(Exception)
            {
                var request = new CreateTableRequest
                {
                    TableName = table,
                    AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Id",
                            AttributeType = ScalarAttributeType.N
                        }
                    },
                    KeySchema = new List<KeySchemaElement>()
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Id",
                            KeyType = KeyType.HASH
                        }
                    },
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 20,
                        WriteCapacityUnits = 20,
                    }
                };

                await client.CreateTableAsync(request);
            }
        }

        public async void TryCreateCommentTable()
        {
            var table = "Comment";

            try
            {
                await client.DescribeTableAsync(table);
            }
            catch (Exception)
            {
                var request = new CreateTableRequest
                {
                    TableName = table,
                    AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Id",
                            AttributeType = ScalarAttributeType.N
                        },
                        new AttributeDefinition
                        {
                            AttributeName = "JokeId",
                            AttributeType = ScalarAttributeType.N
                        }
                    },
                    KeySchema = new List<KeySchemaElement>()
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Id",
                            KeyType = KeyType.HASH
                        },
                        new KeySchemaElement
                        {
                            AttributeName = "JokeId",
                            KeyType = KeyType.RANGE
                        }
                    },
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 20,
                        WriteCapacityUnits = 20,
                    },
                    LocalSecondaryIndexes = new List<LocalSecondaryIndex>()
                    {
                        new LocalSecondaryIndex
                        {
                            IndexName = "index_comment_id_joke_id",
                            KeySchema = new List<KeySchemaElement>()
                            {
                                new KeySchemaElement
                                {
                                    AttributeName = "Id",
                                    KeyType = KeyType.HASH
                                },
                                new KeySchemaElement
                                {
                                    AttributeName = "JokeId",
                                    KeyType = KeyType.RANGE
                                }
                            },
                            Projection = new Projection
                            {
                                ProjectionType = ProjectionType.ALL
                            }
                        }
                    }
                };

                await client.CreateTableAsync(request);
            }
        }

        public async void TryCreateReplyTable()
        {
            var table = "Reply";

            try
            {
                await client.DescribeTableAsync(table);
            }
            catch (Exception)
            {
                var request = new CreateTableRequest
                {
                    TableName = table,
                    AttributeDefinitions = new List<AttributeDefinition>()
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Id",
                            AttributeType =ScalarAttributeType.N
                        },
                        new AttributeDefinition
                        {
                            AttributeName = "CommentId",
                            AttributeType = ScalarAttributeType.N
                        }
                    },
                    KeySchema = new List<KeySchemaElement>()
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Id",
                            KeyType = KeyType.HASH
                        },
                        new KeySchemaElement
                        {
                            AttributeName = "CommentId",
                            KeyType = KeyType.RANGE
                        }
                    },
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 20,
                        WriteCapacityUnits = 20,
                    },
                    LocalSecondaryIndexes = new List<LocalSecondaryIndex>()
                    {
                        new LocalSecondaryIndex
                        {
                            IndexName = "index_reply_id_comment_id",
                            KeySchema = new List<KeySchemaElement>()
                            {
                                new KeySchemaElement
                                {
                                    AttributeName = "Id",
                                    KeyType = KeyType.HASH
                                },
                                new KeySchemaElement
                                {
                                    AttributeName = "CommentId",
                                    KeyType = KeyType.RANGE
                                }
                            },
                            Projection = new Projection
                            {
                                ProjectionType = ProjectionType.ALL
                            }
                        }
                    }
                };

                await client.CreateTableAsync(request);
            }
        }
    }
}
