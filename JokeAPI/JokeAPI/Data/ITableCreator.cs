﻿namespace JokeAPI.Data
{
    public interface ITableCreator
    {
        void TryCreateJokeTable();
        void TryCreateCommentTable();
        void TryCreateReplyTable();
    }
}
