﻿using Amazon.DynamoDBv2.DataModel;
using JokeAPI.Modules;

namespace JokeAPI.Models
{
    [DynamoDBTable("Joke")]
    public class Joke : Serializable
    {
        public Joke()
        {
        }

        public Joke(int id, string? creator, string? text, List<string>? tags, DateTime creationTime)
        {
            Id = id;
            Creator = creator;
            Text = text;
            Tags = tags;
            CreationTime = creationTime;
        }

        [DynamoDBHashKey]
        public int Id { get; set; }
        [DynamoDBProperty]
        public string? Creator { get; set; }
        [DynamoDBProperty]
        public string? Text { get; set; }
        [DynamoDBProperty]
        public List<string>? Tags { get; set; }
        [DynamoDBProperty]
        public DateTime CreationTime { get; set; }
    }
}
