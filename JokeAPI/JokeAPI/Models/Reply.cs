﻿using Amazon.DynamoDBv2.DataModel;

namespace JokeAPI.Models
{
    [DynamoDBTable("Reply")]
    public class Reply
    {
        public Reply()
        {
        }

        public Reply(int id, int commentId, string? creator, string? text)
        {
            Id = id;
            CommentId = commentId;
            Creator = creator;
            Text = text;
        }

        [DynamoDBHashKey]
        public int Id { get; set; }
        [DynamoDBRangeKey]
        public int CommentId { get; set; }
        [DynamoDBProperty]
        public string? Creator { get; set; }
        [DynamoDBProperty]
        public string? Text { get; set; }
    }
}
