﻿using Amazon.DynamoDBv2.DataModel;
using JokeAPI.Modules;

namespace JokeAPI.Models
{
    [DynamoDBTable("Comment")]
    public class Comment : Serializable
    {
        public Comment()
        {
        }

        public Comment(int id, int jokeId, string? creator, string? text, DateTime creationTime)
        {
            Id = id;
            JokeId = jokeId;
            Creator = creator;
            Text = text;
            CreationTime = creationTime;
        }

        [DynamoDBHashKey]
        public int Id { get; set; }
        [DynamoDBRangeKey]
        public int JokeId { get; set; }
        [DynamoDBProperty]
        public string? Creator { get; set; }
        [DynamoDBProperty]
        public string? Text { get; set; }
        [DynamoDBProperty]
        public DateTime CreationTime { get; set; }
    }
}
