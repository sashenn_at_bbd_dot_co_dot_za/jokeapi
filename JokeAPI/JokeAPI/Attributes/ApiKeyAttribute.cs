﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace JokeAPI.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string APIKEY = "ApiKey";

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if(context.HttpContext.Request.Headers.TryGetValue(APIKEY, out var value))
            {
                var settings = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
                var apiKey = settings.GetValue<string>(APIKEY);

                if(apiKey.Equals(value))
                {
                    await next();
                } else
                {
                    context.Result = new ContentResult()
                    {
                        StatusCode = 401,
                        Content = "API Key provided is invalid!!!"
                    };
                }
            } else
            {
                context.Result = new ContentResult()
                {
                    StatusCode = 401,
                    Content = "API Key is not provided!!!"
                };
            }
        }
    }
}
