﻿using JokeAPI.Attributes;
using JokeAPI.Models;
using JokeAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JokeAPI.Controllers
{
    [ApiKey]
    [Route("api/reply")]
    [ApiController]
    public class ReplyController : ControllerBase
    {
        private readonly IReplyService service;

        public ReplyController(IReplyService service)
        {
            this.service = service;
        }

        [HttpPost("create")]
        public IActionResult CreateReply(Reply reply)
        {
            return Ok(service.CreateReply(reply).Result);
        }

        [HttpPut("update")]
        public IActionResult UpdateReply(Reply reply)
        {
            return Ok(service.UpdateReply(reply).Result);
        }

        [HttpDelete("delete")]
        public IActionResult DeleteReply(Reply reply)
        {
            return Ok(service.DeleteReply(reply).Result);
        }

        [HttpGet("get/all")]
        public IActionResult GetReplies(int commentId)
        {
            return Ok(service.GetReplies(commentId).Result);
        }

        [HttpGet("get/id/{id}")]
        public IActionResult GetReplyById(int id)
        {
            return Ok(service.GetReplyById(id).Result);
        }
    }
}
