﻿using JokeAPI.Attributes;
using JokeAPI.Models;
using JokeAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JokeAPI.Controllers
{
    [ApiKey]
    [Route("api/comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService service;

        public CommentController(ICommentService service)
        {
            this.service = service;
        }

        [HttpPost("create")]
        public IActionResult CreateComment(Comment comment)
        {
            return Ok(service.CreateComment(comment).Result);
        }

        [HttpPut("update")]
        public IActionResult UpdateComment(Comment comment)
        {
            return Ok(service.UpdateComment(comment).Result);
        }

        [HttpDelete("delete")]
        public IActionResult DeleteComment(Comment comment)
        {
            return Ok(service.DeleteComment(comment).Result);
        }

        [HttpGet("get/all")]
        public IActionResult GetComments(int jokeId)
        {
            return Ok(service.GetComments(jokeId).Result);
        }

        [HttpGet("get/id/{id}")]
        public IActionResult GetCommentById(int id)
        {
            var comment = service.GetCommentById(id).Result;
            if(comment == null)
            {
                return NotFound("Comment not found!!!");
            }
            return Ok(comment);
            
            
        }
    }
}
