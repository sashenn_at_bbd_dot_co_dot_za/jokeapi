﻿using JokeAPI.Attributes;
using JokeAPI.Models;
using JokeAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JokeAPI.Controllers
{
    [ApiKey]
    [Route("api/joke")]
    [ApiController]
    public class JokeController : ControllerBase
    {
        private readonly IJokeService service;

        public JokeController(IJokeService service)
        {
            this.service = service;
        }

        [HttpPost("create")]
        public IActionResult CreateJoke(Joke joke)
        {
            return Ok(service.CreateJoke(joke).Result);
        }

        [HttpPut("update")]
        public IActionResult UpdateJoke(Joke joke)
        {
            return Ok(service.UpdateJoke(joke).Result);
        }

        [HttpDelete("delete")]
        public IActionResult DeleteJoke(Joke joke)
        {
            return Ok(service.DeleteJoke(joke).Result);
        }

        [HttpGet("get/all")]
        public IActionResult GetJokes()
        {
            return Ok(service.GetJokes().Result);
        }

        [HttpGet("get/id/{id}")]
        public IActionResult GetJokeById(int id)
        {
            var joke = service.GetJokeById(id).Result;
            if(joke == null)
            {
                return NotFound("Joke not found!!!");
            }
            return Ok(joke);
        }

        [HttpGet("get/tag/{tag}")]
        public IActionResult GetJokesByTag(int id)
        {
            var joke = service.GetJokeById(id).Result;
            if(joke == null)
            {
                return NotFound("Joke not found!!!");
            }
            return Ok(joke);
        }
    }
}
